


On linux server:
1. nvidia cuda driver https://developer.nvidia.com/cuda-downloads
2. docker + docker-compose ./docker_with_compose.sh
3. nvidia-docker ./nvidia-docker.sh

Run:
ssh -L 6080:localhost:6080 -L 5900:localhost:5900 user@remote.machine.com
mkdir workspace
docker run -p 127.0.0.1:6080:6080 -p 127.0.0.1:5900:5900 -d --name=deep_learning -v $(pwd)/workspace:/home/ubuntu/shared -it nanotrix/deep-learning:desktop startvnc.sh

To set another initial resolution add: -e RESOLUT=1920x1200

Connect:
Open browser  http://localhost:6080/vnc.html?resize=downscale&autoconnect=1&password=vnc
Or Install vnc-viewer https://www.realvnc.com/en/connect/download/viewer/ and connect to localhost:5900

Update image:
1. Edit dockerfile 
2. docker build -t nanotrix/deep-learning:desktop .
2. docker push nanotrix/deep-learning:desktop